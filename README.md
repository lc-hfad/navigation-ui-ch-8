Implementation of the test drives in chapter 8 of
Head First Android Development, 3rd edition.

#### To import in android studio:

File -> New -> Project from Version Control -> Enter repository URL

#### Jumping between commits:

Git tab on bottom left -> Log tab -> Right click on desired commit -> Checkout Revision
